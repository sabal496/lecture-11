package com.example.lecture11

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.withStyledAttributes
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var ItemsList= mutableListOf<Mymodel>()
    private lateinit var adapter:MyAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private  fun init(){

        adapter= MyAdapter(ItemsList)
        recycleview.layoutManager=LinearLayoutManager(this)
        recycleview.adapter=adapter

        setData()
    }

    private  fun setData(){
            ItemsList.add(Mymodel(R.mipmap.ca760b70976b52578da88e06973af542,"Title  1","wings in moon"))
            ItemsList.add(Mymodel(R.mipmap.image,"Title 2","old man in boat"))
            ItemsList.add(Mymodel(R.mipmap.moon,"Title 3","moon in space"))
            ItemsList.add(Mymodel(R.mipmap.ca760b70976b52578da88e06973af542,"Title  4","wings in moon"))
            ItemsList.add(Mymodel(R.mipmap.image,"Title 5","old man in boat"))
            ItemsList.add(Mymodel(R.mipmap.moon,"Title 6","moon in space"))
            ItemsList.add(Mymodel(R.mipmap.ca760b70976b52578da88e06973af542,"Title  7","wings in moon"))
            ItemsList.add(Mymodel(R.mipmap.image,"Title 8","old man in boat"))
            ItemsList.add(Mymodel(R.mipmap.moon,"Title 9","moon in space"))
            ItemsList.add(Mymodel(R.mipmap.ca760b70976b52578da88e06973af542,"Title  10","wings in moon"))
            ItemsList.add(Mymodel(R.mipmap.image,"Title 11","old man in boat"))
            ItemsList.add(Mymodel(R.mipmap.moon,"Title 12","moon in space"))
    }
}
