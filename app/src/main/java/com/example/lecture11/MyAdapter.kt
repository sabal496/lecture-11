package com.example.lecture11

import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycle_view.view.*

class MyAdapter(val ItemsList:MutableList<Mymodel>) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return ItemsList.size
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return  ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recycle_view,parent,false))
    }



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.onBound()

    }

    inner class  ViewHolder(view :View):RecyclerView.ViewHolder(view){

        private lateinit var model:Mymodel

        fun onBound(){
            model=ItemsList[adapterPosition]
            itemView.imageview.setImageResource(model.image)
            itemView.title.text=model.title
            itemView.describe.text=model.desc

        }

    }

}